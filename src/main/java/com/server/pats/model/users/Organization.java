package com.server.pats.model.users;

import com.server.pats.model.DescribedEntity;
import com.server.pats.model.users.photo.Photo;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@ToString(callSuper = true)
@Table(name = "organizations")
public class Organization extends DescribedEntity {

    @OneToOne
    @JoinColumn(name = "photo_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotNull
    private Photo photo;

    public Organization(int id){
        this.setId(id);
    }
}
