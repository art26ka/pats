package com.server.pats.model.users;

import com.server.pats.model.DescribedEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString(callSuper = true)
@Table(name = "posts")
public class Post extends DescribedEntity {

    public Post(int id){
        this.setId(id);
    }
}
