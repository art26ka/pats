package com.server.pats.model.users;

import com.server.pats.model.NamedEntity;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Set;

@Entity
@Table(name = "roles")
public class Role extends NamedEntity implements GrantedAuthority {

   /* @Transient
    @ManyToMany(mappedBy = "roles")
    private Set<User> users;*/

    @Override
    public String getAuthority() {
        return getName();
    }
}
