package com.server.pats.model.users;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.server.pats.model.NamedEntity;
import com.server.pats.model.users.photo.Photo;
import com.server.pats.model.users.project.Achievement;
import com.server.pats.model.users.project.ProjectTechnology;
import com.server.pats.model.users.project.UserAchievement;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.SQLType;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@ToString(callSuper = true)
@Table(name = "users")
public class User extends NamedEntity {

    @NotBlank
    @Size(max = 10, min=2, message = "Не меньше 2 и не более 10 знаков ")
    private String username;

    @Size(min=2, message = "Не меньше 5 знаков")
    private String password;

    @Transient
    @JsonBackReference
    private String passwordConfirm;

    @OneToOne
    @JoinColumn(name = "photo_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Photo photo;

    @OneToMany(mappedBy = "user")
    @JsonManagedReference
    private List<UserAchievement> userAchievements = new ArrayList<>();

    @ManyToOne
    private Organization organization;

    @ManyToOne
    private Post post;

    @Email
    @NotBlank
    @Size(max = 20)
    private String email;

    @Column(name = "phone")
    @NotBlank
    @Size(max = 20)
    private String phone;


    @Column(name = "entry_date")
    private LocalDate entryDate;

   /* @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;*/
}
