package com.server.pats.model.users.photo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.server.pats.model.NamedEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
@Table(name = "photos")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Photo extends NamedEntity {

    public Photo(int id) {
        this.setId(id);
    }
}
