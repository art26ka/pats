package com.server.pats.model.users.project;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.server.pats.model.DescribedEntity;
import com.server.pats.model.users.photo.ProjectImage;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
@Table(name = "projects")
public class Project extends DescribedEntity {

    @OneToMany(mappedBy = "technology")
    @JsonManagedReference
    private List<ProjectTechnology> technologies = new ArrayList<>();

    @OneToMany(mappedBy = "project")
    @JsonManagedReference
    private List<ProjectImage> images = new ArrayList<>();

    public void addImage(ProjectImage projectImage) {
        projectImage.setProject(this);
        images.add(projectImage);
    }

    public void removeSymptom(ProjectImage projectImage) {
        images.remove(projectImage);
    }

}
