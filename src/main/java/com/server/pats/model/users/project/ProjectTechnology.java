package com.server.pats.model.users.project;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.server.pats.model.BaseEntity;


import lombok.*;

import javax.persistence.*;



@Entity
@Getter
@Setter
@ToString(callSuper = true)
@Table(name = "project_technologies")
public class ProjectTechnology extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    @JsonBackReference
    private Project project;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "technology_id")
    @JsonManagedReference
    private Technology technology;
}
