package com.server.pats.model.users.project;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.server.pats.model.DescribedEntity;
import com.server.pats.model.users.photo.Photo;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@ToString(callSuper = true)
@Table(name = "technologies")
public class Technology extends DescribedEntity {

    @OneToOne
    @JoinColumn(name = "photo_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotNull
    private Photo photo;

    @OneToMany(mappedBy = "technology")
    @JsonBackReference
    private List<ProjectTechnology> technologies = new ArrayList<>();

}
