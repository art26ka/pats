package com.server.pats.repository.user.photo;

import com.server.pats.model.users.photo.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoRepository extends JpaRepository<Photo, Integer> {
}
