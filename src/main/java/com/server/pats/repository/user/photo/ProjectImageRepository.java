package com.server.pats.repository.user.photo;

import com.server.pats.model.users.photo.ProjectImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectImageRepository extends JpaRepository<ProjectImage, Integer> {
}
