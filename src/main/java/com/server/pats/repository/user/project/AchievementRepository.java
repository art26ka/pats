package com.server.pats.repository.user.project;


import com.server.pats.model.users.project.Achievement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AchievementRepository extends JpaRepository<Achievement, Integer> {
}
