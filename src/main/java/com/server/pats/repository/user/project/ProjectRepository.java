package com.server.pats.repository.user.project;

import com.server.pats.model.users.project.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Integer> {
}
