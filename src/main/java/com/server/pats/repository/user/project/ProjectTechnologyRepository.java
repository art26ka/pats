package com.server.pats.repository.user.project;

import com.server.pats.model.users.project.ProjectTechnology;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectTechnologyRepository extends JpaRepository<ProjectTechnology,Integer> {
}
