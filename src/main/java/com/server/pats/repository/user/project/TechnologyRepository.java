package com.server.pats.repository.user.project;


import com.server.pats.model.users.project.Technology;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnologyRepository extends JpaRepository<Technology, Integer> {
}
