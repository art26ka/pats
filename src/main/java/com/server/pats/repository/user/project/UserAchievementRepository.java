package com.server.pats.repository.user.project;

import com.server.pats.model.users.project.UserAchievement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAchievementRepository extends JpaRepository<UserAchievement, Integer> {
}
