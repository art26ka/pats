package com.server.pats.service;



import com.server.pats.model.BaseEntity;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@AllArgsConstructor
public abstract class BaseService<T> implements InterfaceService<T> {
    
    private final JpaRepository<T, Integer> repository;

    @Override
    public void create(T object) {
        repository.save(object);
    }

    @Override
    public List<T> readALL(){
        return repository.findAll();
    }

    @Override
    public T readById(int id){
        return repository.findById(id).get();
    }

    @Override
    public boolean update(T object,  int id)
    {
        if(repository.existsById(id)) {
            ((BaseEntity)object).setId(id);
            repository.save(object);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id)
    {
        if (repository.existsById(id))
        {
            repository.deleteById(id);
            return  true;
        }
        return false;
    }


}
