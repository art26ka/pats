package com.server.pats.service;


import java.util.List;

public interface InterfaceService<T> {
    /**
     * Создает объект
     * @param object - класса объекта для создания
     */
    void create(T object);
    /**
     * @return список всех объектов типа
     */
    List<T> readALL();
    /**
     * Возвращает объект по его ID
     * @param id - ID объекта
     * @return - объект с заданным ID
     */
    T readById(int id);
    /**
     * Обновляет объект с заданным ID,
     * в соответствии с переданным объектом
     * @param object - объект, в соответсвии с которым нужно обновить данные
     * @param id - ID объекта, который нужно обновить
     * @return - true если данные были обновлены, иначе false
     */
    boolean update(T object, int id);
    /**
     * Удаляет объект с заданным ID
     * @param id - ID объекта, который нужно удалить
     * @return - true если объект был удален, иначе false
     */
    boolean delete(int id);
}
