package com.server.pats.service.user;

import com.server.pats.model.users.Organization;
import com.server.pats.repository.OrganizationRepository;
import com.server.pats.service.BaseService;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class OrganizationService extends BaseService<Organization> {

    public OrganizationService(OrganizationRepository repository) {
        super(repository);
    }
}
