package com.server.pats.service.user;

import com.server.pats.model.users.Post;
import com.server.pats.repository.PostRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class PostService extends BaseService<Post> {

    public PostService(PostRepository repository) {
        super(repository);
    }
}
