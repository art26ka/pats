package com.server.pats.service.user;

import com.server.pats.model.users.User;
import com.server.pats.repository.UserRepository;
import com.server.pats.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService extends BaseService<User>  {
    @Autowired
    UserRepository repository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository repository) {
        super(repository);
    }

    public boolean checkUsername(String username) {
        try{
            return repository.findByUsername(username) != null;
        }
        catch (Exception ex){
            return false;
        }
    }

    public void encodePasswordAndCreateUser(User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        create(user);
    }

    public boolean comparePassword(String decodedPassword, String encodedPassword){
        return bCryptPasswordEncoder.matches(bCryptPasswordEncoder.encode(decodedPassword), encodedPassword);
    }

    @Override
    public void create(User user) {
        repository.save(user);
    }

    @Override
    public List<User> readALL() {
        return repository.findAll();
    }


    @Override
    public User readById(int id) {
        return repository.findById(id).get();
    }

    public User readByUsername(String username) throws UsernameNotFoundException{
        User user = repository.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        else
            return user;
    }

    @Override
    public boolean update(User user, int id) {
        if(repository.existsById(id)) {
            user.setId(id);
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            repository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id)  {
        if (repository.existsById(id))
        {
            repository.deleteById(id);
            return  true;
        }
        return false;
    }

}
