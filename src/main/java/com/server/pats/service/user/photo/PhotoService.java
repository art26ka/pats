package com.server.pats.service.user.photo;

import com.server.pats.model.users.photo.Photo;
import com.server.pats.repository.user.photo.PhotoRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class PhotoService extends BaseService<Photo> {

    public PhotoService(PhotoRepository repository) {
        super(repository);
    }
}
