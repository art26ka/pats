package com.server.pats.service.user.photo;

import com.server.pats.model.users.photo.ProjectImage;
import com.server.pats.repository.user.photo.ProjectImageRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class ProjectImageService extends BaseService<ProjectImage> {
    public ProjectImageService(ProjectImageRepository repository) {
        super(repository);
    }
}
