package com.server.pats.service.user.project;

import com.server.pats.model.users.project.Achievement;
import com.server.pats.repository.user.project.AchievementRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class AchievementService extends BaseService<Achievement> {

    public AchievementService(AchievementRepository repository) {
        super(repository);
    }
}
