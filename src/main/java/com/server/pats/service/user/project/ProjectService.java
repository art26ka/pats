package com.server.pats.service.user.project;

import com.server.pats.model.users.project.Project;
import com.server.pats.repository.user.project.ProjectRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class ProjectService extends BaseService<Project> {

    public ProjectService(ProjectRepository repository) {
        super(repository);
    }
}
