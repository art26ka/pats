package com.server.pats.service.user.project;

import com.server.pats.model.users.project.ProjectTechnology;
import com.server.pats.repository.user.project.ProjectTechnologyRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class ProjectTechnologyService extends BaseService<ProjectTechnology> {

    public ProjectTechnologyService(ProjectTechnologyRepository repository) {
        super(repository);
    }
}
