package com.server.pats.service.user.project;

import com.server.pats.model.users.project.Technology;
import com.server.pats.repository.user.project.TechnologyRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class TechnologyService extends BaseService<Technology> {

    public TechnologyService(TechnologyRepository repository) {
        super(repository);
    }
}
