package com.server.pats.service.user.project;

import com.server.pats.model.users.project.UserAchievement;
import com.server.pats.repository.user.project.UserAchievementRepository;
import com.server.pats.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class UserAchievementService extends BaseService<UserAchievement> {

    public UserAchievementService(UserAchievementRepository repository) {
        super(repository);
    }

}
