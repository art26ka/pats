package com.server.pats.web.user;

import com.server.pats.model.users.Organization;
import com.server.pats.service.user.OrganizationService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/organizations")
@Api(tags="Organizations controller")
public class OrganizationController extends BaseController<Organization> {

    public OrganizationController(OrganizationService service) {
        super(service);
    }
}
