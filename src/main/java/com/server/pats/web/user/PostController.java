package com.server.pats.web.user;

import com.server.pats.model.users.Post;
import com.server.pats.service.user.PostService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/posts")
@Api(tags="Posts controller")
public class PostController extends BaseController<Post> {

    public PostController(PostService service) {
        super(service);
    }
}
