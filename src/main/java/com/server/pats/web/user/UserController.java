package com.server.pats.web.user;

import com.server.pats.model.users.User;

import com.server.pats.service.user.OrganizationService;
import com.server.pats.service.user.PostService;
import com.server.pats.service.user.UserService;
import com.server.pats.service.user.photo.PhotoService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;


@RestController
@RequestMapping("/users")
@Api(tags="Users controller")
public class UserController extends BaseController<User> {
    @Autowired
    UserService userService;
    @Autowired
    PostService postService;
    @Autowired
    OrganizationService organizationService;
    @Autowired
    PhotoService photoService;

    public UserController(UserService service) {
        super(service);
    }

    public void initializationDate(User userForm){
        userForm.setPost(postService.readById(userForm.getPost().getId()));
        userForm.setPhoto(photoService.readById(userForm.getPhoto().getId()));
        userForm.setOrganization(organizationService.readById(userForm.getOrganization().getId()));
        userForm.setEntryDate(LocalDate.now());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<?> create (@Valid @RequestBody User userForm)
    {

        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())){
            return new ResponseEntity<>("Passwords are not identical", HttpStatus.BAD_REQUEST);
        }

        if(!userService.checkUsername(userForm.getName())) {
            initializationDate(userForm);
            userService.encodePasswordAndCreateUser(userForm);
        }
        else
            return new ResponseEntity<>("Not-free username",  HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/{username}/{password}")
    public ResponseEntity<?> authentication(@PathVariable String username, @PathVariable String password){
        User user = userService.readByUsername(username);
        if(user != null) {
            return userService.comparePassword(password, user.getPassword())
                    ? new ResponseEntity<>(user, HttpStatus.OK)
                    : new ResponseEntity<>("The password are not correct", HttpStatus.NOT_FOUND);
        }
            else return new ResponseEntity<>("The login are not detection", HttpStatus.NOT_FOUND);

    }

}
