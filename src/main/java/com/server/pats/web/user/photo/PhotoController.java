package com.server.pats.web.user.photo;

import com.server.pats.model.users.photo.Photo;

import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/photos")
@Api(tags="Photos controller")
public class PhotoController extends BaseController<Photo> {

    public PhotoController(com.server.pats.service.user.photo.PhotoService service) {
        super(service);
    }
}
