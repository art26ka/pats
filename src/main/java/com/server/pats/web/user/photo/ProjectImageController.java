package com.server.pats.web.user.photo;

import com.server.pats.model.users.photo.ProjectImage;
import com.server.pats.service.user.photo.ProjectImageService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project_images")
@Api(tags="Project images controller")
public class ProjectImageController extends BaseController<ProjectImage> {
    public ProjectImageController(ProjectImageService service) {
        super(service);
    }
}
