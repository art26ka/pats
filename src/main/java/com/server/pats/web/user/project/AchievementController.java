package com.server.pats.web.user.project;

import com.server.pats.model.users.project.Achievement;
import com.server.pats.service.user.project.AchievementService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/achievements")
@Api(tags="Achievements controller")
public class AchievementController extends BaseController<Achievement> {

    public AchievementController(AchievementService service) {
        super(service);
    }
}
