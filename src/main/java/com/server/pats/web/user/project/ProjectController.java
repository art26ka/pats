package com.server.pats.web.user.project;

import com.server.pats.model.users.project.Project;
import com.server.pats.service.user.project.ProjectService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
@Api(tags="Projects controller")
public class ProjectController extends BaseController<Project> {

    public ProjectController(ProjectService service) {
        super(service);
    }
}
