package com.server.pats.web.user.project;

import com.server.pats.model.users.project.ProjectTechnology;
import com.server.pats.service.user.project.ProjectTechnologyService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project_technologies")
@Api(tags="Project technologies controller")
public class ProjectTechnologyController extends BaseController<ProjectTechnology> {

    public ProjectTechnologyController(ProjectTechnologyService service) {
        super(service);
    }
}
