package com.server.pats.web.user.project;

import com.server.pats.model.users.project.Technology;
import com.server.pats.service.user.project.TechnologyService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/technologies")
@Api(tags="Technologies controller")
public class TechnologyController extends BaseController<Technology> {

    public TechnologyController(TechnologyService service) {
        super(service);
    }
}
