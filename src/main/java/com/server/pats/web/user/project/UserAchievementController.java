package com.server.pats.web.user.project;


import com.server.pats.model.users.project.UserAchievement;
import com.server.pats.service.user.project.UserAchievementService;
import com.server.pats.web.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user_achievements")
@Api(tags="User achievements controller")
public class UserAchievementController extends BaseController<UserAchievement> {
    public UserAchievementController(UserAchievementService service) {
        super(service);
    }
}
