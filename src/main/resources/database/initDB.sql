create table if not exists pats.photos
(
    id   serial not null
        constraint photos_pk
            primary key,
    name text   not null
);

alter table pats.photos
    owner to postgres;

create table if not exists pats.users
(
    id           serial                   not null
        constraint profile_pk
            primary key,
    name         text                     not null,
    nickname     text                     not null,
    email        text                     not null,
    number_phone text                     not null,
    organization integer                  not null,
    photo        integer
        constraint profiles_photo_fk
            references pats.photos
            on delete cascade,
    entry_date   date default date(now()) not null,
    post         integer                  not null
);

alter table pats.users
    owner to postgres;

create table if not exists pats.organizations
(
    id          serial  not null
        constraint organization_pk
            primary key,
    name        text    not null,
    description text    not null,
    photo       integer not null
        constraint organizations_photo_fk
            references pats.photos
            on delete cascade
);

alter table pats.organizations
    owner to postgres;

create table if not exists pats.technologies
(
    id          serial  not null
        constraint technologies_pk
            primary key,
    name        text    not null,
    description integer not null,
    photo       integer
);

alter table pats.technologies
    owner to postgres;

create table if not exists pats.projects
(
    id                   integer,
    name                 serial not null
        constraint projects_pk
            primary key,
    project_technologies integer,
    description          integer
);

alter table pats.projects
    owner to postgres;

create table if not exists pats.project_technologies
(
    id         integer,
    technology integer,
    project    integer
);

alter table pats.project_technologies
    owner to postgres;

create table if not exists pats.chats
(
    id           integer,
    interlocutor integer,
    owner_chat   integer
);

alter table pats.chats
    owner to postgres;

create table if not exists pats.chat_messages
(
    id        serial not null
        constraint chat_messages_pk
            primary key,
    owner     boolean,
    message   text,
    date_time timestamp default now(),
    chat      integer
);

alter table pats.chat_messages
    owner to postgres;

create table if not exists pats.orders
(
    id          serial  not null
        constraint orders_pk
            primary key,
    name        integer not null,
    description integer not null,
    photo       integer
);

alter table pats.orders
    owner to postgres;

create table if not exists pats.posts
(
    id          serial not null
        constraint posts_pk
            primary key,
    name        text   not null,
    description text   not null
);

alter table pats.posts
    owner to postgres;

create table if not exists pats.achievements
(
    id          serial  not null
        constraint achievements_pk
            primary key,
    name        integer not null,
    description integer not null,
    photo       integer not null
        constraint achievements_photo_fk
            references pats.photos
            on delete cascade
);

alter table pats.achievements
    owner to postgres;